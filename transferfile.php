<?php
$lang = !empty($_GET['lang']) && in_array($_GET['lang'], ['en', 'br']) ?
    $_GET['lang'] : 'en';
if (!empty($_FILES)) {
    $dir = 'uploads/';
    $filename = $_FILES['file']['name'];
    $targetFile = function($filename) use ($dir) {
        return $dir . basename($filename);
    };
    if (file_exists($targetFile($filename)) &&
        filesize($targetFile($filename)) <> $_FILES['file']['size']) {
        list($name, $extension) = explode('.', $filename);
        $filename = $name . '_' . date('Ymdhis') . '.' . $extension;
    }
    if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFile($filename))) {
        echo $lang == 'en' ?
            'File has been uploaded' :
            'Arquivo foi enviado';
    } else {
        echo $lang == 'en' ?
            'Upload failed' :
            'Upload falhou, tente novamente';
    }
}
$title = $lang == 'en' ? 'Transfer File' : 'Transferir Arquivo';
?>
<!DOCTYPE html>
<html>
<?php
if (file_exists('components/partials/header.php'))
    include_once('components/partials/header.php');
?>
<body class='text-center'>
    <?php if ($lang == 'en'): ?>
    <h1>TRANSFER FILES TO SERVER</h1>
    <?php else: ?>
    <h1>TRANSFERIR ARQUIVO PARA O SERVIDOR</h1>
    <?php endif; ?>
    <form method="post" enctype="multipart/form-data">
        <input type='file' name='file'>
        <input type='submit' name="submit" value='Upload'>
    </form>
</body>
</html>
