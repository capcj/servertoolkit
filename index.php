<?php
$featuresList = [
    [
        'filename' => 'transferfile.php',
        'name' => [
            'en' => 'Transfer Files to Server',
            'br' => 'Transferir Arquivos'
        ],
        'description' => [
            'en' => 'Transfer files to the uploads/ path in the server to further use',
            'br' => 'Transfere arquivos para a pasta uploads no servidor para uso futuro',
        ]
    ],
    [
        'filename' => 'timeout.php',
        'name' => [
            'en' => 'Test Browser Timeout',
            'br' => 'Testar timeout do browser'
        ],
        'description' => [
            'en' => 'Make routines to ensure max browser timeout per requisition',
            'br' => 'Faz rotinas para garantir qual o maximo timeout do browser por requisicao'
        ]
    ],
    [
        'filename' => 'repository.php',
        'name' => [
            'en' => 'Files Repository',
            'br' => 'Repositorio de Arquivos'
        ],
        'description' => [
            'en' => 'Make server upload dir available',
            'br' => 'Disponibiliza o diretorio upload'
        ]
    ],
    [
        'filename' => 'stats.php',
        'name' => [
            'en' => 'Server Stats',
            'br' => 'Status do Servidor'
        ],
        'description' => [
            'en' => 'Server Status Monitor',
            'br' => 'Monitoramento do Servidor'
        ]
    ]
];

$lang = !empty($_GET['lang']) && in_array($_GET['lang'], ['en', 'br']) ?
    $_GET['lang'] : 'en';

$title = empty($_GET['title']) ? 'Server Toolkit' : $_GET['title'];
$showFeatures = empty($_GET['show']) ? [] : explode(',', $_GET['show']);
?>
<!DOCTYPE html>
<html>
<?php
if (file_exists('components/partials/header.php'))
    include_once('components/partials/header.php');
?>
<body class='text-center'>
    <h1><?= $title; ?></h1>
    <?php if ($lang == 'en'): ?>
    <h2>If you want to use some of the builtin server features, see below:</h2>
    <?php endif; ?>
    <div class='flexbox'>
    <?php
    foreach ($featuresList as $featureIndex => $feature) {
        if (!empty($showFeatures) && !in_array($featureIndex+1, $showFeatures)) continue;
         echo "<a href='{$feature['filename']}?lang={$lang}' title='{$feature['description'][$lang]}'>",
        $feature['name'][$lang],
        '</a>';
    }
    ?>
    </div>
</body>
</html>
