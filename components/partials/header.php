<head>
    <title><?= empty($title) ? 'Server Toolkit' : $title; ?></title>
    <link rel="stylesheet" type="text/css" href="components/css/style.css">
    <?php if (!strpos($_SERVER['SCRIPT_NAME'], 'index')): ?>
    <div id='nav'>
        <a href="javascript:history.back()">&#8592;</a>
    </div>
    <?php endif; ?>
</head>
