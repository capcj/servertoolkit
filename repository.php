<?php
/*
 * Repository
 *
 * A program made to easily download files that is inside
 * some path in the server
 *
 * Has the GET parameters:
 * lang = en | br (default: en);
 * path = allows to enter inside uploads/ subdirectories (default: uploads/);
 *
 *
 */
$lang = !empty($_GET['lang']) && in_array($_GET['lang'], ['en', 'br']) ?
    $_GET['lang'] : 'en';
$title = $lang == 'en' ? 'Files Repository' : 'Repositorio de Arquivos';
$path = 'uploads/' . (empty($_GET['path']) ? '' : $_GET['path']);
$path = str_replace('..', '.', $path);
?>
<!DOCTYPE html>
<html>
<?php
if (file_exists('components/partials/header.php'))
    include_once('components/partials/header.php');
?>
<body class='text-center'>
    <?php if ($lang == 'en'): ?>
    <h1>Files Repository</h1>
    <?php else: ?>
    <h1>Repositorio de Arquivos</h1>
    <?php endif; ?>
    <div class='flexbox'>
    <?php
    if (!file_exists($path)) {
        mkdir("uploads");
    }

    function listRepository($repository = [], $buffer = '', $path = '', $lang = 'en', $lastIndex = 0) {
        $totalFiles = count($repository);
        if ($totalFiles == 0) {
            $repoIsEmptyTag = "<b>&nbsp;{$path}&nbsp;</b>&nbsp;";
            $repoIsEmptyTag .= $lang == 'en' ?
                "is empty" :
                "esta vazio";
            return $buffer == '' ?
                $repoIsEmptyTag : $buffer;
        } else {
            $file = $repository[$lastIndex];
            if ($file !== '' &&
                $file !== '.' &&
                $file !== '..' &&
                !is_dir($path . $file)) {
                    $buffer .= "<a href='{$path}/{$file}' download>{$file}</a>";
                }
            unset($repository[$lastIndex]);
            $lastIndex++;
            return listRepository($repository, $buffer, $path, $lang, $lastIndex);
        }
    }
    echo listRepository(scandir($path), '', $path, $lang);
    ?>
    </div>
</body>
</html>
