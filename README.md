# Server Toolkit
A basic PHP toolkit to help the basic routines of a sysadmin.

In my daily tasks sometimes I need basic stuff to solve some problems, so there is.

The tools are multilingual (en, br) and has some filtering features by GET:
- lang: br | en (default: en);
- title: string (default: Server Toolkit);
- show: choose the features you want to be seen (default: all);

Examples:
- ?lang=br&title=Teste (portuguese language with title Teste);
- ?lang=br&title=Teste&show=1,2 (portuguese language with title Teste and show only features 1 and 2);

## repository.php

- lang: br | en (default: en);
- path: path/inside/uploads/ (default: uploads/);

# Rules:
- Must be light, clean, dependency zero so far;
- Must have compatibility in CLI browsers;
- KISS;
- Try to stick in something professed by the [Unix Philosophy](https://en.wikipedia.org/wiki/Unix_philosophy "Pure Awesomeness") - even with my lack of expertise/time;
