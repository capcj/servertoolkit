<?php
$title = 'Server Stats';
/*
 *
 * getSymbolByQuantity by tularis@php.net from php.net
 *
 *
 */
function getSymbolByQuantity($bytes) {
    $symbols = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB',
'YiB');
    $exp = floor(log($bytes)/log(1024));

    return sprintf('%.2f '.$symbols[$exp], ($bytes/pow(1024, floor($exp))));
}
function getUptime() {
    $uptime = explode(',',`uptime`);
    $translateRawUptime = [
        function($str) {
            $buffer = explode('up', $str);
            if (!isset($buffer[1])) {
                $buffer = explode(' ', $buffer[0]);
            }
            return "Uptime: {$buffer[1]}";
        },
        function($str) {
            return "Users: {$str}";
        },
        function($str) {
            $str = str_replace(':', ':<br>', $str);
            return "{$str} in the last 1 minute";
        },
        function($str) {
            return "{$str} in the last 5 minutes";
        },
        function($str) {
            return "{$str} in the last 15 minutes";
        }
    ];
    $buffer = '';
    foreach($uptime as $index => $val) {
        $buffer .= $translateRawUptime[$index]($val) . "<br>";
    }
    return $buffer;
}
function getNetworkInterfaces() {
    $interfaces = `ip link show`;
    if ($interfaces === NULL) {
        $interfaces = explode(
            ' ',
            `ifconfig -a | grep "flags="`
        );
        if ($interfaces === NULL) {
            $interfaces = '0 interfaces found';
        } else {
            foreach ($interfaces as $index => $val) {
                if (strpos($val, ':') !== false) {
                    $interfaces[$index] = preg_replace(
                        '/\s+/', '<br>', $val
                    );
                } else if (strpos($val, '<') !== false) {
                    $interfaces[$index] = htmlspecialchars($val);
                }
            }
            $interfaces = implode(' ', $interfaces);
        }
    }
    return $interfaces;
}
function mountTable($headArr, $bodyArr, $bodyExceptArr, $bodyBreakPoint) {
    $processHeader = function() use ($headArr) {
        $headBuffer = '';
        foreach ($headArr as $th) {
            $headBuffer .= "<th>{$th}</th>";
        }
        return $headBuffer;
    };
    $buffer = '<table class="text-center center">
                    <thead>
                        <tr>' .
                        $processHeader() .
                        '</tr>
                    </thead>
                    <tbody>
        ';
    $column = 0;
    $buffer .= '<tr>';
    foreach ($bodyArr as $val) {
        if (in_array("$column", $bodyExceptArr)) {
            $column++;
            continue;
        }
        $buffer .= "<td>{$val}</td>";
        if ($column == $bodyBreakPoint) {
            $column = 0;
            $buffer .= '</tr><tr>';
        } else {
            $column++;
        }
    }
    $buffer .= '</tbody></table>';
    return $buffer;
}
function getProcesses($procName = 'php') {
    $head = [
        'PID',
        '%CPU',
        '%MEM',
        'STARTED',
        'TIME',
        'COMMAND'
    ];
    $shell = preg_replace(
        '/\s+/', ' ', `ps xuca | grep {$procName}`
    );
    $shellArr = array_filter(
        explode(' ', $shell),
        'strlen'
    );
    $shellExceptions = [0, 4, 5, 6, 7];
    return mountTable($head, $shellArr, $shellExceptions, 10);
}
function getDiskSpace() {
    $diskspace = `df -h`;
    if ($diskspace !== NULL) {
        $diskspace = preg_replace(
            '/\s+/', ' ', $diskspace
        );
        $diskspace = array_filter(
            explode(' ', $diskspace),
            'strlen'
        );
        $head = [];
        foreach ($diskspace as $index => $val) {
            if ($index < 6) {
                if ($index !== 5) {
                    $head[] = $val;
                } else {
                    $head[] = "{$val} {$diskspace[$index+1]}";
                    unset($diskspace[$index+1]);
                }
                unset($diskspace[$index]);
            } else {
                break;
            }
        }
        $diskspace = mountTable($head, $diskspace, [], 5);
    } else {
        $diskspace =
        getSymbolByQuantity(disk_free_space('/'))
        . '/' .
        getSymbolByQuantity(disk_total_space('/'));
    }
    return $diskspace;
}
function getPathsSize() {
    $paths = ['/boot', '/home', '/tmp'];
    $buffer = '';
    foreach ($paths as $path) {
        if (!is_dir($path)) {
            continue;
        }
        $buffer .= "<p>{$path} - " .
                getSymbolByQuantity(filesize($path)) .
                '</p>';
    }
    return $buffer;
}
$stats = [
    [
        'name' => 'Server Name',
        'value' => $_SERVER['SERVER_NAME']
    ],
    [
        'name' => 'Server OS',
        'value' => php_uname('a')
    ],
    [
        'name' => 'Server IP',
        'value' => $_SERVER['SERVER_ADDR']
    ],
    [
        'name' => 'Server Date',
        'value' => `date`
    ],
    [
        'name' => 'PHP Date',
        'value' => date(DATE_RFC2822)
    ],
    [
        'name' => 'Server Uptime',
        'value' => getUptime()
    ],
    [
        'name' => 'PHP_PID',
        'value' => getmypid()
    ],
    [
        'name' => 'PHP Processes',
        'value' => getProcesses('php')
    ],
    [
        'name' => 'CPU',
        'value' => str_replace(
            'CPU states:',
            '',
            `top | head -3 | tail -1`
        )
    ],
    [
        'name' => 'Processes Running',
        'value' => explode('up', `top | head -2 | tail -1`)[0]
    ],
    [
        'name' => 'Memory',
        'value' => str_replace(
            'Memory:',
            '',
            `top | head -4 | tail -1`
        )
    ],
    [
        'name' => 'Network Interfaces',
        'value' => getNetworkInterfaces()
    ],
    [
        'name' => 'Disk Space',
        'value' => getDiskSpace()
    ],
    [
        'name' => 'Paths Size',
        'value' => getPathsSize()
    ],
];
function generateTableBody($stats = [], $buffer = '', $lastIndex = 0) {
    $totalStats = count($stats);
    if ($totalStats == 0) {
        return $buffer;
    } else {
        $stat = $stats[$lastIndex];
        if (!empty($stat['value'])) {
            $buffer .= "
                <tr>
                    <td>
                        {$stat['name']}
                    </td>
                    <td>
                        {$stat['value']}
                    </td>
                </tr>
            ";
        }
        unset($stats[$lastIndex]);
        $lastIndex++;
        return generateTableBody($stats, $buffer, $lastIndex);
    }
}
?>
<!DOCTYPE html>
<html>
<?php
if (file_exists('components/partials/header.php'))
    include_once('components/partials/header.php');
?>
<body class='text-center'>
    <h1>Server Stats</h1>
    <table class='center' border='1'>
        <thead>
            <tr>
                <th>Stat</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
            <?= generateTableBody($stats, ''); ?>
        </tbody>
    </table>
</body>
</html>
