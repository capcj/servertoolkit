<?php
$title = 'Timeout Testing';
?>
<!DOCTYPE html>
<html>
<?php
if (file_exists('components/partials/header.php'))
    include_once('components/partials/header.php');
?>
<body class='text-center'>
<h1>TIMEOUT TESTING</h1>
<form name='job_form' method="get">
    <input name='job_time' id='job-time' value='<?= empty($_GET['job_time']) ? 5 : $_GET['job_time']; ?>'>
    <input name='submit' id='submit' type='submit' value='Send Job'>
</form>
<?php
if (empty($_GET['job_time'])) {
    return false;
}
function calcJobs($sec = 5, $buffer = '') {
    sleep(1);
    $sec--;
    $buffer .= 'processing...<br>';
    if ($sec == 0) {
        return
        "<h2>Total Request Time: {$_GET['job_time']}s<br></h2>{$buffer}";
    } else {
        return calcJobs($sec, $buffer);
    }
};
echo calcJobs($_GET['job_time']);
?>
</body>
</html>
